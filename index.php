<?php

echo ord('f'); //102
echo"</br>";
echo chr(103);

$version = 8;
$langue = "fr";
$masque = "PHP %d est disponible en %s";
printf($masque, $version, $langue);

/* Masques de printf()
* %s chaîne de caractères
* %d nombre entier signé
* %u nombre entier non signé
* %f nombre à virgule flottante
* %b binaire
* %o octal
* %x hexadécimal
* %X hexadécimal avec majuscules
* %% affiche %
*/
$chaine = "Ceci est une phrase.";
echo $chaine[1];
// Avant PHP8, on pouvait également écrire $chaine{1}



?>